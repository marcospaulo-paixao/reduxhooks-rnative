import React, { useState } from "react";
import { Button, Input } from "@ui-kitten/components";
import { View } from "react-native";
import { getCourses, postCourse, useAppDispatch } from "@store/index";
import { styles } from "@components/CourseForm/styles";
import { PlusIcon } from "@shared/icons";

export const CourseForm = () => {
  const dispatch = useAppDispatch();
  const [courseTitle, setCourseTitle] = useState("");

  const handlePostCourse = async () => {
    dispatch(postCourse({ title: courseTitle })).then(() => {
      updateCourses().then(() => {
        cleanForm();
      });
    });
  };

  function updateCourses() {
    return dispatch(getCourses());
  }

  function cleanForm() {
    setCourseTitle("");
  }

  return (
    <>
      <View style={styles.container}>
        <Input
          style={styles.input}
          placeholder="Title"
          value={courseTitle}
          onChangeText={(nextValue) => setCourseTitle(nextValue)}
        />
        <Button
          appearance="outline"
          status="success"
          style={styles.button}
          onPress={handlePostCourse}
          accessoryLeft={PlusIcon}
        />
      </View>
    </>
  );
};
