import { createSlice } from "@reduxjs/toolkit";
import {
  deleteCourse,
  getCourses,
  postCourse,
} from "@store/slice/course/CourseChunck";

const CourseSlice = createSlice({
  name: "courses",
  initialState: {
    list: {
      isLoading: false,
      status: "",
      values: [],
    },
  },

  extraReducers: (builder) => {
    function updateState(state: any, isLoading: boolean, status: string) {
      state.list.isLoading = isLoading;
      state.list.status = status;
    }

    builder
      .addCase(getCourses.pending, (state) => {
        updateState(state, true, "pending");
      })
      .addCase(getCourses.fulfilled, (state, { payload }: any) => {
        updateState(state, false, "success");
        state.list.values = payload;
      })
      .addCase(getCourses.rejected, (state) => {
        updateState(state, false, "failed");
      });

    builder
      .addCase(postCourse.pending, (state) => {
        updateState(state, true, "pending");
      })
      .addCase(postCourse.fulfilled, (state) => {
        updateState(state, false, "success");
      })
      .addCase(postCourse.rejected, (state) => {
        updateState(state, false, "failed");
      });

    builder
      .addCase(deleteCourse.pending, (state) => {
        updateState(state, true, "pending");
      })
      .addCase(deleteCourse.fulfilled, (state) => {
        updateState(state, false, "success");
      })
      .addCase(deleteCourse.rejected, (state) => {
        updateState(state, false, "failed");
      });
  },
  reducers: {},
});

export default CourseSlice.reducer;
