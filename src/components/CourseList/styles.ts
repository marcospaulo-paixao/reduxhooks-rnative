import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    width: "90%",
  },
  button: {
    height: 45,
    width: 80,
  },
});
