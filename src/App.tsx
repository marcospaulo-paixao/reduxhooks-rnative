import React from "react";

import { store } from "@store/index";
import { Provider } from "react-redux";

import * as eva from "@eva-design/eva";
import { ApplicationProvider, IconRegistry } from "@ui-kitten/components";
import { EvaIconsPack } from "@ui-kitten/eva-icons";

import { StatusBar } from "expo-status-bar";
import { View } from "react-native";

import { CourseForm, CourseList } from "@components/index";
import { styles } from "@shared/styles";

export default function App() {
  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <Provider store={store}>
        <ApplicationProvider {...eva} theme={eva.light}>
          <StatusBar style="auto" />
          <View style={styles.container}>
            <CourseForm />
            <CourseList />
          </View>
        </ApplicationProvider>
      </Provider>
    </>
  );
}
