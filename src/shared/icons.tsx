import { Icon } from "@ui-kitten/components";
import React from "react";

export const PlusIcon = (props: any) => <Icon {...props} name="plus" />;

export const TrashIcon = (props: any) => <Icon {...props} name="trash" />;
