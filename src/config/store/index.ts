import { configureStore } from "@reduxjs/toolkit";
import CourseSlice from "@store/slice/course/CourseSlice";
import { useDispatch } from "react-redux";

export const store = configureStore({
  reducer: {
    courses: CourseSlice,
  },
});

type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export type RootState = ReturnType<typeof store.getState>;

export * from "@store/slice/course/CourseChunck";
