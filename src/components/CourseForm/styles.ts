import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    width: 303,
  },
  button: {
    width: 80,
    height: 45,
    marginLeft: 10,
  },
});
