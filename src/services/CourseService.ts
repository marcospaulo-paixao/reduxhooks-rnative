import { AxiosInstance } from "@config/axios";

export interface ICourse {
  id?: number;
  title: string;
}

export class CourseService {
  private static endPoint = "courses";
  static get = () => {
    return AxiosInstance.get(this.endPoint);
  };
  static post = (course: ICourse) => {
    return AxiosInstance.post(this.endPoint, course);
  };

  static delete = (id: number) => {
    return AxiosInstance.delete(`${this.endPoint}/${id}`);
  };
}
