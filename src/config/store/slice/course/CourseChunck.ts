import { createAsyncThunk } from "@reduxjs/toolkit";
import { CourseService } from "@services/CourseService";

export const getCourses = createAsyncThunk(
  "courses/getCourses",
  async (arg, thunkAPI) => {
    try {
      const { data } = await CourseService.get();
      return thunkAPI.fulfillWithValue(data);
    } catch (error: any) {
      return thunkAPI.rejectWithValue(error.toString());
    }
  }
);

export const postCourse = createAsyncThunk(
  "courses/postCourses",
  async (arg: any, thunkAPI) => {
    try {
      const { data } = await CourseService.post({ ...arg });
      return thunkAPI.fulfillWithValue(data);
    } catch (error: any) {
      return thunkAPI.rejectWithValue(error.toString());
    }
  }
);

export const deleteCourse = createAsyncThunk(
  "courses/deleteCourse",
  async (arg: any, thunkAPI) => {
    try {
      const { data } = await CourseService.delete(arg);
      return thunkAPI.fulfillWithValue(data);
    } catch (error: any) {
      return thunkAPI.rejectWithValue(error.toString());
    }
  }
);
