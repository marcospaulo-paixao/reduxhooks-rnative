import React, { useEffect } from "react";
import { Button, List, ListItem, Text } from "@ui-kitten/components";
import { useSelector } from "react-redux";
import {
  deleteCourse,
  getCourses,
  RootState,
  useAppDispatch,
} from "@store/index";
import { styles } from "@components/CourseList/styles";
import { TrashIcon } from "@shared/icons";

export const CourseList = () => {
  const dispatch = useAppDispatch();
  const data: any = useSelector(
    (state: RootState) => state.courses.list.values
  );

  useEffect(() => {
    updateCourses();
  }, [dispatch]);

  const handleDeleteCourse = async (index: any) => {
    dispatch(deleteCourse(index));
    updateCourses();
  };

  function updateCourses() {
    dispatch(getCourses());
  }

  const renderItem = ({ item }: any) => (
    <ListItem
      accessoryLeft={() => <Text>{item.id}</Text>}
      accessoryRight={() => (
        <Button
          appearance="outline"
          status="danger"
          style={styles.button}
          onPress={() => handleDeleteCourse(item.id)}
          accessoryLeft={TrashIcon}
        />
      )}
      title={`${item.title}`}
    />
  );

  return <List style={styles.container} data={data} renderItem={renderItem} />;
};
